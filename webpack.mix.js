let mix = require('laravel-mix');

mix
    .sass('scss/app.scss', 'css')
    .js('src/app.js', 'dist/js')
    .setPublicPath('dist')
    .browserSync({server: 'dist', port: 8080});
